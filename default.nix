{
	nixpkgs ? import <nixpkgs> {
		overlays = [
			(self: super: {
				rustc = (super.rustc.override {
					stdenv = self.stdenv.override {
						targetPlatform = super.stdenv.targetPlatform // {
							parsed = {
								cpu = { name = "wasm32"; };
								vendor = {name = "unknown";};
								kernel = {name = "unknown";};
								abi = {name = "unknown";};
							};
						};
					};
				}).overrideAttrs (attrs: {
					configureFlags = attrs.configureFlags ++ ["--set=build.docs=false"];
				});
			})
		];
	},
	naersk ? nixpkgs.pkgs.callPackage (fetchTarball "https://github.com/nmattia/naersk/archive/master.tar.gz") {},
}:
with nixpkgs;

lib.genAttrs
	[
		"bitmap"
		"bloom"
		"compressed-list"
		"list"
		"prefix-compressed-list"
		"prefix-skip"
		"skip"
	]
	(feature: let
		build = naersk.buildPackage {
			root = pkgs.nix-gitignore.gitignoreSource [
				"*.nix"
				".*"
			] ./.;
			buildInputs = with pkgs; [
				lld
			];
			RUSTFLAGS = "-C linker=lld";
			cargoBuildOptions = def: def ++ [
				"--features=impl-${feature}"
				"--target=wasm32-unknown-unknown"
			];
			copyLibs = true;
		};
		opt = pkgs.runCommand "word-store-${feature}-opt.wasm" {} ''
			${pkgs.binaryen}/bin/wasm-opt ${build}/lib/word_store.wasm -o $out -Os
		'';
		wasm-bindgen = pkgs.runCommand "word-store-${feature}" {} ''
			${pkgs.wasm-bindgen-cli}/bin/wasm-bindgen ${opt} --browser --out-dir $out --out-name word-store-${feature}
		'';
		zstd = pkgs.runCommand "word-store-${feature}.wasm.zstd" {} ''
			mkdir $out
			${pkgs.zstd}/bin/zstd -19 <${wasm-bindgen}/*.wasm >$out/${feature}.wasm.zstd
		'';
		in zstd)
