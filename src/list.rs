#[derive(Debug,Default)]
pub struct List;

impl crate::WordStore for List {
	fn serialize(
		words: &[[u8; 5]],
		mut out: impl std::io::Write,
	) -> std::io::Result<()> {
		for word in words {
			out.write_all(&word[..])?;
		}
		Ok(())
	}

	fn contains(data: &[u8], word: [u8; 5]) -> bool {
		let words = data.len() / word.len();
		debug_assert_eq!(data.len() % word.len(), 0);

		let mut min = 0;
		let mut max = words;
		while min != max {
			let cur = min / 2 + max / 2;
			let off = cur*word.len();
			let candidate = &data[off..][..5];
			match candidate.cmp(&word) {
				std::cmp::Ordering::Less => min = cur + 1,
				std::cmp::Ordering::Equal => return true,
				std::cmp::Ordering::Greater => max = cur,
			}
		}

		false
	}
}

#[test]
fn test() {
	crate::test::<List>();
}
