mod bitmap;
mod bloom;
mod compressed_list;
mod list;
mod prefix_compressed_list;
mod prefix_skip;
mod skip;

pub trait WordStore {
	fn serialize(
		words: &[[u8; 5]],
		out: impl std::io::Write,
	) -> std::io::Result<()>;

	fn contains(data: &[u8], word: [u8; 5]) -> bool;
}

/// Returns the index of the word in the ordered list of all possible words.
fn index(word: [u8; 5]) -> u32 {
	let mut i: u32 = 0;
	for c in word {
		debug_assert!((b'a'..=b'z').contains(&c));
		i *= 26;
		i += (c - b'a') as u32;
	}
	i
}

const CHARS: std::ops::RangeInclusive<u8> = b'a'..=b'z';

fn all_words() -> impl Iterator<Item=[u8; 5]> {
	CHARS.into_iter()
		.flat_map(|c| CHARS.map(move |c1| [c, c1]))
		.flat_map(|[c0, c1]| CHARS.map(move |c2| [c0, c1, c2]))
		.flat_map(|[c0, c1, c2]| CHARS.map(move |c3| [c0, c1, c2, c3]))
		.flat_map(|[c0, c1, c2, c3]| CHARS.map(move |c4| [c0, c1, c2, c3, c4]))
}

fn words() -> Vec<[u8; 5]> {
	let mut words = Vec::with_capacity(1024);
	let file = std::fs::File::open("/home/kevincox/Downloads/wordle-sorted.txt").unwrap();
	for line in std::io::BufRead::lines(std::io::BufReader::new(file)) {
		let line = line.unwrap();
		let line = line.as_bytes();
		assert_eq!(line.len(), 5);
		words.push([
			line[0],
			line[1],
			line[2],
			line[3],
			line[4],
		]);
	}
	words
}

#[cfg(test)]
fn non_words() -> Vec<[u8; 5]> {
	let real_words = words();
	let capacity = 26usize.pow(5) - real_words.len();
	let mut words = Vec::with_capacity(capacity);
	let mut actual = real_words.into_iter().peekable();
	for w in all_words() {
		if actual.peek() == Some(&w) {
			actual.next();
			continue
		}
		words.push(w);
	}
	assert_eq!(capacity, words.len());
	rand::seq::SliceRandom::shuffle(
		&mut words[..],
		&mut <rand::rngs::StdRng as rand::SeedableRng>::seed_from_u64(0));
	words
}

#[cfg(test)]
fn test<Store: WordStore>() {
	let mut words = words();
	let non_words = non_words();

	let mut data = Vec::new();
	Store::serialize(&words, &mut data).expect("Serializing failed");

	rand::seq::SliceRandom::shuffle(
		&mut words[..],
		&mut <rand::rngs::StdRng as rand::SeedableRng>::seed_from_u64(0));

	let mut accept_count = 0;
	let accept_start = std::time::Instant::now();
	for w in &words {
		assert!(
			Store::contains(&data, *w),
			"Expected match {:?}",
			String::from_utf8_lossy(w));
		accept_count += 1;
	}
	let accept_elapsed = accept_start.elapsed();

	let mut reject_count = 0;
	let reject_start = std::time::Instant::now();
	for w in &non_words {
		assert!(
			!Store::contains(&data, *w),
			"Unexpected match {:?}",
			String::from_utf8_lossy(w));
		reject_count += 1;
	}
	let reject_elapsed = reject_start.elapsed();

	println!("Accepted in {} ns on average.", (accept_elapsed / accept_count).as_nanos());
	println!("Rejected in {} ns on average.", (reject_elapsed / reject_count).as_nanos());
	println!("Size: {:.3} KiB", data.len() as f32 / 1024.0);
	#[cfg(feature="zstd")] {
		let size = zstd::encode_all(std::io::Cursor::new(&data), 0).unwrap().len();
		println!("zstd default: {:.3} KiB", size as f32 / 1024.0);
		let size = zstd::encode_all(std::io::Cursor::new(&data), *zstd::compression_level_range().end()).unwrap().len();
		println!("zstd max: {:.3} KiB", size as f32 / 1024.0);
	}
}

#[no_mangle]
pub fn contains(data: &[u8], word: [u8; 5]) -> bool {
	#[cfg(feature="impl-bitmap")] return bitmap::Bitmap::contains(data, word);
	#[cfg(feature="impl-bloom")] return bloom::Bloom::contains(data, word);
	#[cfg(feature="impl-compressed-list")] return compressed_list::CompressedList::contains(data, word);
	#[cfg(feature="impl-list")] return list::List::contains(data, word);
	#[cfg(feature="impl-prefix-compressed-list")] return prefix_compressed_list::PrefixCompressedList::contains(data, word);
	#[cfg(feature="impl-prefix-skip")] return prefix_skip::PrefixSkip::contains(data, word);
	#[cfg(feature="impl-skip")] return skip::Skip::contains(data, word);
}
