#[derive(Debug,Default)]
pub struct Bitmap;

fn index(word: [u8; 5]) -> (usize, u8) {
	let i = crate::index(word) as usize;
	(i / 8, 1u8 << (i % 8))
}

impl crate::WordStore for Bitmap {
	fn serialize(
		words: &[[u8; 5]],
		mut out: impl std::io::Write,
	) -> std::io::Result<()> {
		let mut bitmap = [0; 26usize.pow(5)/8+1];
		for word in words {
			let (byte, bit) = index(*word);
			bitmap[byte] |= bit;
		}
		out.write_all(&bitmap[..])?;
		Ok(())
	}

	fn contains(data: &[u8], word: [u8; 5]) -> bool {
		let (byte, bit) = index(word);
		data[byte] & bit != 0
	}
}

#[test]
fn test() {
	crate::test::<Bitmap>();
}
