#[derive(Debug,Default)]
pub struct Bloom;

const BITS: u32 = 21;
const HASHES: u32 = 6;

fn bits(word: [u8; 5]) -> impl Iterator<Item=(usize, u8)> {
	let mut h = std::collections::hash_map::DefaultHasher::default();
	std::hash::Hasher::write(&mut h, &word);
	let h = std::hash::Hasher::finish(&mut h);
	let h1 = h as u32;
	let h2 = (h >> 32) as u32;

	(0..HASHES)
		.map(move |i| h1.wrapping_add(h2.wrapping_mul(i)) & ((1 << BITS)-1))
		// .inspect(move |i| eprintln!("{} = {}", String::from_utf8_lossy(&word), i))
		.map(|i| (i as usize / 8, 1u8 << (i % 8)))
}

impl crate::WordStore for Bloom {
	fn serialize(
		words: &[[u8; 5]],
		mut out: impl std::io::Write,
	) -> std::io::Result<()> {
		let mut bitmap = [0; 2usize.pow(BITS) / 8];
		for word in words {
			for (byte, bit) in bits(*word) {
				bitmap[byte] |= bit;
			}
		}
		let mut bits = 0;
		for byte in &bitmap {
		// 	print!("{:02x}", byte);
			bits += byte.count_ones();
		}
		// eprintln!();
		eprintln!("Occupancy {}", bits as f32 / (bitmap.len()*8) as f32);
		out.write_all(&bitmap[..])?;
		Ok(())
	}

	fn contains(data: &[u8], word: [u8; 5]) -> bool {
		for (byte, bit) in bits(word) {
			if data[byte] & bit == 0 {
				return false
			}
		}
		true
	}
}

#[test]
fn test() {
	crate::test::<Bloom>();
}
