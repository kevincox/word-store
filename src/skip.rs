#[derive(Debug,Default)]
pub struct Skip;

impl crate::WordStore for Skip {
	fn serialize(
		words: &[[u8; 5]],
		mut out: impl std::io::Write,
	) -> std::io::Result<()> {
		let mut base = 0;
		for word in words {
			let this = crate::index(*word);
			let distance = this - 1 - base;
			let encoding = [
				0x80 | (distance >> 21) as u8,
				0x80 | (distance >> 14) as u8,
				0x80 | (distance >> 7) as u8,
				distance as u8 & 0x7F
			];
			out.write_all(
				if distance >> 28 > 0 { unreachable!() }
				else if distance >> 21 > 0 { &encoding[0..] }
				else if distance >> 14 > 0 { &encoding[1..] }
				else if distance >> 7 > 0 { &encoding[2..] }
				else { &encoding[3..] })?;
			base = this;
		}
		Ok(())
	}

	fn contains(data: &[u8], word: [u8; 5]) -> bool {
		let mut remaining = crate::index(word);
		let mut data = data.iter().cloned();
		while let Some(mut b) = data.next() {
			let mut distance = b as u32 & 0b0111_1111;
			while b & 0b1000_0000 != 0 {
				b = data.next().unwrap();
				distance <<= 7;
				distance |= b as u32 & 0b0111_1111;
			}
			distance += 1;
			match remaining.checked_sub(distance.into()) {
				Some(0) => return true,
				Some(r) => remaining = r,
				None => break,
			}
		}
		false
	}
}

#[test]
fn test() {
	crate::test::<Skip>();
}
