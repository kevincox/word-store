#[derive(Debug,Default)]
pub struct CompressedList;

fn compress(word: [u8; 5]) -> [u8; 3] {
	let mut i = crate::index(word);
	i += 2u32.pow(24) - 26u32.pow(5); // Get as much entropy as possible in the first byte.
	let bytes = i.to_be_bytes();
	debug_assert_eq!(bytes[0], 0);
	[
		bytes[1],
		bytes[2],
		bytes[3],
	]
}

impl crate::WordStore for CompressedList {
	fn serialize(
		words: &[[u8; 5]],
		mut out: impl std::io::Write,
	) -> std::io::Result<()> {
		for word in words {
			out.write_all(&compress(*word)[..])?;
		}
		Ok(())
	}

	fn contains(data: &[u8], word: [u8; 5]) -> bool {
		let compressed = compress(word);

		let words = data.len() / 3;
		debug_assert_eq!(words * 3, data.len());

		let mut min = 0;
		let mut max = words;
		while min != max {
			let cur = min / 2 + max / 2;
			let off = cur*3;
			let candidate = &data[off..][..3];
			match candidate.cmp(&compressed) {
				std::cmp::Ordering::Less => min = cur + 1,
				std::cmp::Ordering::Equal => return true,
				std::cmp::Ordering::Greater => max = cur,
			}
		}

		false
	}
}

#[test]
fn test() {
	crate::test::<CompressedList>();
}
