#[derive(Debug,Default)]
pub struct PrefixCompressedList;

fn jump_index(c1: u8, c2: u8) -> usize {
	(c1 - b'a') as usize * 26 + (c2 - b'a') as usize
}

fn compress(suffix: [u8; 3]) -> [u8; 2] {
	let mut i = 0;
	for c in suffix {
		i *= 26;
		i += (c - b'a') as u16;
	}
	i.to_le_bytes()
}

const JUMP_END: usize = 26usize.pow(2);

impl crate::WordStore for PrefixCompressedList {
	fn serialize(
		words: &[[u8; 5]],
		mut out: impl std::io::Write,
	) -> std::io::Result<()> {
		let mut buf = Vec::new();
		buf.extend([0; JUMP_END]); // Prefix jumps.

		for word in words {
			buf[jump_index(word[0], word[1])] += 1;
			buf.extend(compress([word[2], word[3], word[4]]));
		}

		out.write_all(&buf)
	}

	fn contains(data: &[u8], word: [u8; 5]) -> bool {
		let ji = jump_index(word[0], word[1]);
		let skip = data[..ji].iter().map(|b| *b as usize).sum();
		let count = data[ji];
		let target = compress([word[2], word[3], word[4]]);
		data[JUMP_END..]
			.chunks(2)
			.skip(skip)
			.take(count.into())
			.find(|bytes| bytes == &target)
			.is_some()
	}
}

#[test]
fn test() {
	crate::test::<PrefixCompressedList>();
}
