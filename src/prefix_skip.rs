#[derive(Debug,Default)]
pub struct PrefixSkip;

fn index(word: [u8; 4]) -> u32 {
	let mut i: u32 = 0;
	for c in word {
		i *= 26;
		i += (c - b'a') as u32;
	}
	i
}

impl crate::WordStore for PrefixSkip {
	fn serialize(
		words: &[[u8; 5]],
		mut out: impl std::io::Write,
	) -> std::io::Result<()> {
		let mut counts = [0u16; 26];
		let mut last_first = b'.';
		let mut last = 0;
		for word in words {
			if word[0] != last_first {
				last_first = word[0];
				last = 0;
			}

			let mut len = 1;
			let i = index([word[1], word[2], word[3], word[4]]);
			let mut distance = i - 1 - last;
			while distance > 0x7F {
				len += 1;
				distance >>= 7;
			}
			counts[(word[0] - b'a') as usize] += len;
			last = i;
		}
		for count in counts {
			out.write_all(&count.to_le_bytes())?;
		}

		let mut last_first = b'.';
		let mut last = 0;
		for word in words {
			if word[0] != last_first {
				last_first = word[0];
				last = 0;
			}

			let i = index([word[1], word[2], word[3], word[4]]);
			let distance = i - 1 - last;
			let encoding = [
				0x80 | (distance >> 21) as u8,
				0x80 | (distance >> 14) as u8,
				0x80 | (distance >> 7) as u8,
				distance as u8 & 0x7F
			];
			out.write_all(
				if distance >> 28 > 0 { unreachable!() }
				else if distance >> 21 > 0 { &encoding[0..] }
				else if distance >> 14 > 0 { &encoding[1..] }
				else if distance >> 7 > 0 { &encoding[2..] }
				else { &encoding[3..] })?;
			last = i;
		}
		Ok(())
	}

	fn contains(data: &[u8], word: [u8; 5]) -> bool {
		let first = (word[0] - b'a') as usize;
		let skip: usize = data
			.chunks(2)
			.take(first)
			.map(|b| u16::from_le_bytes([b[0], b[1]]) as usize)
			.sum();
		let count = u16::from_le_bytes([data[first*2], data[first*2+1]]) as usize;

		let mut remaining = index([word[1], word[2], word[3], word[4]]);
		let mut data = data[(26*2 + skip)..][..count].iter().cloned();
		while let Some(mut b) = data.next() {
			let mut distance = b as u32 & 0b0111_1111;
			while b & 0b1000_0000 != 0 {
				b = data.next().unwrap();
				distance <<= 7;
				distance |= b as u32 & 0b0111_1111;
			}
			distance += 1;
			match remaining.checked_sub(distance.into()) {
				Some(0) => return true,
				Some(r) => remaining = r,
				None => break,
			}
		}
		false
	}
}

#[test]
fn test() {
	crate::test::<PrefixSkip>();
}
